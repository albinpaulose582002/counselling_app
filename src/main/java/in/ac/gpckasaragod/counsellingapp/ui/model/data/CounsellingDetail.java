/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CounsellingDetail {
     private String clientName;
    private String problems;
    private String advice;

    public CounsellingDetail(String clientName, String problems, String advice) {
        this.clientName = clientName;
        this.problems = problems;
        this.advice = advice;
    }
    private static final Logger LOG = Logger.getLogger(CounsellingDetail.class.getName());

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProblems() {
        return problems;
    }

    public void setProblems(String problems) {
        this.problems = problems;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public Integer getId() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
