/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ClientDetail {
     private Integer id;
    private String clientName;
    private String clientAddress;
    private String mobileNo; 

    public ClientDetail(Integer id, String clientName, String clientAddress, String mobileNo) {
        this.id = id;
        this.clientName = clientName;
        this.clientAddress = clientAddress;
        this.mobileNo = mobileNo;
    }
    private static final Logger LOG = Logger.getLogger(ClientDetail.class.getName());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
