/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.service;
import in.ac.gpckasaragod.counsellingapp.ui.model.data.ClientDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface ClientDetailsService {
    
    public String saveClientDetailForm(String clientName,String clientAddress,String mobileNo);
    public ClientDetail readClientDetail(Integer id);
    public List<ClientDetail> getAllClientDetailses();
    public String updateClientDetails(Integer id,String clientName,String clientAddress,String mobileNo);
    public String deleteClientDetails(Integer id);   


    

   
}
