/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.service.impl;

import in.ac.gpckasaragod.counsellingapp.service.CounsellingDetailsService;
import in.ac.gpckasaragod.counsellingapp.ui.model.data.CounsellingDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CounsellingDetailsServiceImpl extends ConnectionServiceImpl implements CounsellingDetailsService{
   @Override
    public String saveCounsellingDetailForm(String clientName, String problems, String advice) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "INSERT INTO COUNSELLING_DETAILS(CLIENT_NAME,PROBLEMS,ADVICE)Values '+' ('"+clientName+"','"+problems+"','"+advice+"')";
          System.err.println("Query:"+query);
          int status = statement.executeUpdate(query);
          if(status !=1){
              return "Save failed";
          }else{
              return "Saved successfully";
          }
      
          
      } catch (SQLException ex) {
          Logger.getLogger(CounsellingDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
          return "Save failed"; 
    }

    @Override
    public CounsellingDetail readCounsellingDetail(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        CounsellingDetail counsellingDetail = null;
          try{
              Connection connection = getConnection();
              Statement statement = connection.createStatement();
              String query = "SELECT * FROM COUNSELLING_DETAILS WHERE ID="+id;
              ResultSet resultset = statement.executeQuery(query);
              
              while (resultset.next()){
               String clientName = resultset.getString("CLIENT_NAME");
               String problems = resultset.getString("PROBLEMS");
               String advice = resultset.getString("ADVICE");
               counsellingDetail = new CounsellingDetail(clientName,problems,advice);
               }
              
              }catch (SQLException ex) {
                Logger.getLogger(CounsellingDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
              }
               return counsellingDetail;
    }

    @Override
    public List<CounsellingDetail> getAllCounsellingDetailses() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        List<CounsellingDetail>CounsellingDetail= new ArrayList<>();
    try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM COUNSELLING_DETAILS";
        ResultSet resultset = statement.executeQuery(query);
        
        while (resultset.next()){
               Integer id = resultset.getInt("ID");
               String clientName = resultset.getString("CLIENT_NAME");
               String problems = resultset.getString("PROBLEMS");
               String advice = resultset.getString("ADVICE");
               CounsellingDetail counsellingDetail = new CounsellingDetail(clientName,problems,advice);
               CounsellingDetail.add(counsellingDetail);
               
    }
       }catch (SQLException ex) {
          Logger.getLogger(CounsellingDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
       return CounsellingDetail;
    }

    

    @Override
    public String updateCounsellingDetails(Integer id, String clientName, String problems, String advice) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          
          String query = "UPDATE COUNSELLING_DETAILS SET CLIENT_NAME='"+clientName+"',PROBLEMS='"+problems+"',ADVICE='"+advice+"' WHERE ID="+id;
          System.out.print(query);
          int update = statement.executeUpdate(query);
          if(update!=1)
              return "Updated failed";
          else
              return "Updated successfully";
      } catch (SQLException ex) {
          Logger.getLogger(CounsellingDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
          return "Update failed";
      }
    }

    @Override
    public String deleteCounsellingDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection = getConnection();
            String query = "DELETE FROM COUNSELLING_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        
    }   catch (SQLException ex) {
            Logger.getLogger(CounsellingDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Delete failed";

    
    }
}

    

    

    

