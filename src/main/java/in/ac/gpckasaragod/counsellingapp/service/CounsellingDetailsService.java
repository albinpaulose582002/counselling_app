/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.service;

import in.ac.gpckasaragod.counsellingapp.ui.model.data.CounsellingDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface CounsellingDetailsService {
    public String saveCounsellingDetailForm(String clientName, String problems, String advice);
    public CounsellingDetail readCounsellingDetail(Integer id);
    public List<CounsellingDetail> getAllCounsellingDetailses();
    public String updateCounsellingDetails(Integer id,String clientName,String problems,String advice);
    public String deleteCounsellingDetails(Integer id);
}
