/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.counsellingapp.service.impl;

import in.ac.gpckasaragod.counsellingapp.service.ClientDetailsService;

import in.ac.gpckasaragod.counsellingapp.ui.model.data.ClientDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ClientDetailsServiceImpl extends ConnectionServiceImpl implements ClientDetailsService{
    @Override
    public String saveClientDetailForm(String clientName, String clientAddress, String mobileNo) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "INSERT INTO CLIENT_DETAILS(CLIENT_NAME,CLIENT_ADDRESS,MOBILE_NO) VALUES ('"+clientName+"','"+clientAddress+"','"+mobileNo+"')";
          System.err.println("Query:"+query);
          int status = statement.executeUpdate(query);
          if(status !=1){
              return "Save failed";
          }else{
              return "Saved successfully";
          }
      
          
      } catch (SQLException ex) {
          Logger.getLogger(ClientDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
          return "Save failed"; 
    }

    @Override
    public ClientDetail readClientDetail(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        ClientDetail clientDetails = null;
          try{
              Connection connection = getConnection();
              Statement statement = connection.createStatement();
              String query = "SELECT * FROM CLIENT_DETAILS WHERE ID="+id;
              ResultSet resultset = statement.executeQuery(query);
              
              while (resultset.next()){   
               String clientName = resultset.getString("CLIENT_NAME");
               String clientAddress = resultset.getString("CLIENT_ADDRESS");
               String mobileNo = resultset.getString("MOBILE_NO");
               clientDetails = new ClientDetail(id,clientName,clientAddress,mobileNo);
               }
              
              }catch (SQLException ex) {
                Logger.getLogger(ClientDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
              }
               return clientDetails;
    }

    @Override
    public List<ClientDetail> getAllClientDetailses() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        List<ClientDetail>ClientDetails= new ArrayList<>();
    try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM CLIENT_DETAILS";
        ResultSet resultset = statement.executeQuery(query);
        
        while (resultset.next()){
               Integer id = resultset.getInt("ID");
               String clientName = resultset.getString("CLIENT_NAME");
               String clientAddress = resultset.getString("CLIENT_ADDRESS");
               String mobileNo = resultset.getString("MOBILE_NO");
               ClientDetail clientDetail = new ClientDetail(id,clientName,clientAddress,mobileNo);
               ClientDetails.add(clientDetail);
               
    }
       }catch (SQLException ex) {
          Logger.getLogger(ClientDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
       return ClientDetails;
    }

    

    @Override
    public String updateClientDetails(Integer id, String clientName, String clientAddress, String mobileNo) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          
          String query = "UPDATE CLIENT_DETAILS SET CLIENT_NAME='"+clientName+"',CLIENT_ADDRESS='"+clientAddress+"',MOBILE_NO='"+mobileNo+"' WHERE ID="+id;
          System.out.print(query);
          int update = statement.executeUpdate(query);
          if(update!=1)
              return "Updated failed";
          else
              return "Updated successfully";
      } catch (SQLException ex) {
          Logger.getLogger(ClientDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
          return "Update failed";
      }
    }

    @Override
    public String deleteClientDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection = getConnection();
            String query = "DELETE FROM CLIENT_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        
    }   catch (SQLException ex) {
            Logger.getLogger(ClientDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Delete failed";

    
  }

    }


  

